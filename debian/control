Source: uw-imap
Priority: optional
Section: mail
Origin: WTF
Bugs: mailto:wtf@mirbsd.org
Maintainer: Thorsten Glaser <t.glaser@tarent.de>
X-Original-Maintainer: Magnus Holmgren <holmgren@debian.org>
Standards-Version: 4.5.1
Build-Depends: cdbs,
 debhelper,
 dh-buildinfo,
 libpam-dev,
 krb5-multidev,
 comerr-dev,
 libssl-dev,
 d-shlibs,
 perl,
 po-debconf
Rules-Requires-Root: binary-targets
Vcs-Git: https://salsa.debian.org/tg/uw-imap.git
Vcs-Browser: https://salsa.debian.org/tg/uw-imap
Homepage: https://en.wikipedia.org/wiki/UW_IMAP

Package: uw-imapd
Architecture: any
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Conflicts: ${cdbs:Conflicts}
Provides: ${cdbs:Provides}
Description: remote mail folder access server using IMAP4rev1
 IMAP (Internet Message Access Protocol) is a method of accessing
 electronic messages kept on a (possibly shared) mail server.
 .
 The UW (University of Washington) IMAP toolkit provides the c-client
 mail-handling library supporting various network transport methods,
 file system storage formats, and authentication and authorization
 methods.
 .
 This package contains imapd, an IMAP4rev1 server daemon which uses the
 c-client library.

Package: ipopd
Architecture: any
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Conflicts: ${cdbs:Conflicts}
Provides: ${cdbs:Provides}
Description: POP2 and POP3 mail server
 IMAP (Internet Message Access Protocol) is a method of accessing
 electronic messages kept on a (possibly shared) mail server.
 .
 The UW (University of Washington) IMAP toolkit provides the c-client
 mail-handling library supporting various network transport methods,
 file system storage formats, and authentication and authorization
 methods.
 .
 This package contains ipopd, a POP2 and POP3 server daemon which uses
 the c-client library.

Package: libc-client2007e-dev
Section: libdevel
Architecture: any
Depends: ${devlibs:Depends},
 ${misc:Depends},
 libc-client2007e (= ${binary:Version})
Conflicts: libc-client-dev
Replaces: libc-client-dev
Provides: libc-client-dev
Description: c-client library for mail protocols - development files
 IMAP (Internet Message Access Protocol) is a method of accessing
 electronic messages kept on a (possibly shared) mail server.
 .
 The UW (University of Washington) IMAP toolkit provides the c-client
 mail-handling library supporting various network transport methods,
 file system storage formats, and authentication and authorization
 methods.
 .
 This package contains the static c-client library and development
 headers.

Package: libc-client2007e
Section: libs
Architecture: any
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Suggests: ${cdbs:Suggests}
Description: c-client library for mail protocols - library files
 IMAP (Internet Message Access Protocol) is a method of accessing
 electronic messages kept on a (possibly shared) mail server.
 .
 The UW (University of Washington) IMAP toolkit provides the c-client
 mail-handling library supporting various network transport methods,
 file system storage formats, and authentication and authorization
 methods.
 .
 This package contains the shared c-client library.

Package: mlock
Architecture: any
Depends: ${misc:Depends},
 ${shlibs:Depends}
Description: mailbox locking program
 IMAP (Internet Message Access Protocol) is a method of accessing
 electronic messages kept on a (possibly shared) mail server.
 .
 The UW (University of Washington) IMAP toolkit provides the c-client
 mail-handling library supporting various network transport methods,
 file system storage formats, and authentication and authorization
 methods.
 .
 This package contains a program to lock mailbox files to avoid
 corruption.

Package: uw-mailutils
Architecture: any
Depends: ${misc:Depends},
 ${shlibs:Depends}
Description: c-client support programs
 IMAP (Internet Message Access Protocol) is a method of accessing
 electronic messages kept on a (possibly shared) mail server.
 .
 The UW (University of Washington) IMAP toolkit provides the c-client
 mail-handling library supporting various network transport methods,
 file system storage formats, and authentication and authorization
 methods.
 .
 This package contains helper tools for the libc-client library:
  - mailutil: utility program which helps manage email mailboxes (both
              local and IMAP/POP3/NNTP);
  - dmail:    MDA (Mail Delivery Agent) for use with procmail;
  - tmail:    MDA for use with the system mailer (such as Sendmail or Postfix).
